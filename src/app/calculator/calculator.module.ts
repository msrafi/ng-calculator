import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NumsComponent } from './nums/nums.component';
import { DisplayComponent } from './display/display.component';



@NgModule({
  declarations: [NumsComponent, DisplayComponent],
  exports: [NumsComponent, DisplayComponent],
  imports: [
    CommonModule
  ]
})
export class CalculatorModule { }
