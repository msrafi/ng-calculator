import { Component, OnInit } from "@angular/core";
import { CalculatorService } from "../calculator.service";

@Component({
  selector: "app-nums",
  templateUrl: "./nums.component.html",
  styleUrls: ["./nums.component.css"]
})
export class NumsComponent implements OnInit {
  // Info of all changes
  private info: any = {
    disVal: "0",
    fVal: "",
    sVal: "",
    ops: ""
  };

  firstVal: number = 0;
  secondVal: number = 0;
  totalVal: number = 0;

  // to hold the numbers
  fNumber = [];
  sNumber = [];
  numFlag: boolean = true;

  constructor(private _calculatorService: CalculatorService) {}

  ngOnInit() {}

  // Everything Happens Here
  calcClicked($event) {
    // First check what button is clicked
    let x = $event.target.value;

    // Clicked non-button
    if (x === undefined) {
      return;
    }

    // if Operative buttons are clicked
    if (x === "+" || x === "-" || x === "*" || x === "%") {
      console.log("op Clicked", x);

      this.info.ops = x;
      // move the final numbers
      this.numFlag
        ? (this.firstVal = parseFloat(this.info.disVal))
        : (this.secondVal = parseFloat(this.info.disVal));

      switch (x) {
        case ".":
          // this.combineNumbers(x);
          break;
        case "%":
          this.numFlag = false;
          break;
        case "+":
          this.numFlag = false;
          // this.plus(x);
          break;
        case "-":
          this.numFlag = false;
          break;
        // return this.info.firstOperand -= secondOp;
        case "*":
          this.numFlag = false;
          break;
        // return this.info.firstOperand *= secondOp;
        case "/":
          this.numFlag = false;
          break;
        // return this.info.firstOperand /= secondOp;
        // case "=":
        //   this.numFlag = false;
        //   break;
        // // return secondOp;
        // case "c":
        //   this.numFlag = false;
        // this.clearAll();
        // return secondOp;
      }
      this.updateObj();
      console.log(this);
    } else {
      this.numFlag = true;
      // console.log("numbers Pressed", x);

      // if(this.info.fVal !== "" && this.info.sVal !== "") {
      //   this.info.fVal = this.info.disVal;
      //   this.info.sVal = "";
      //   this.info.disVal = "0";
      //   // this.clearInfo();
      //   this.numFlag = true;

      // }
      this.combineNumbers(x);
      // // Publish the values
      // this.postMessage();

      console.log(this.info);
    }

    this.postMessage();
  }

  public updateObj(){
    if(this.firstVal !== 0 && this.secondVal !== 0){
      this.firstVal = this.totalVal;
      this.info.fVal = this.totalVal.toString(); 
    }
  }

  public combineNumbers(x: string) {
    if (this.numFlag) {
      this.fNumber.push(x);
      this.firstVal = parseFloat(this.fNumber.join(""));
      this.info.disVal = this.fNumber.join("");
    } else {
      this.sNumber.push(x);
      this.secondVal = parseFloat(this.sNumber.join(""));
      this.info.disVal = this.sNumber.join("");
    }
    
  }

  public postMessage() {
    this._calculatorService.updateValue(this.info);
  }

  // Method when + button is clicked
  public plus(x) {
    // if (this.info.fVal === "" && this.info.sVal === "") {
    //   this.info.fVal = this.info.disVal;
    //   this.info.disVal = "0";
    // }else if(this.info.fVal !== "" && this.info.sVal === "") {
    //   this.info.sVal = this.info.disVal;
    //   this.info.disVal = (this.firstVal + this.secondVal).toString();
    // }else if(this.info.fVal !== "" && this.info.sVal !== "") {
    //   this.info.fVal = this.info.disVal;
    //   this.info.sVal = '';
    //   this.info.disVal = '0';
    //   // this.clearInfo();

    // }

    console.log(this);
  }
  public minus() {
    return;
  }
  public divide() {
    return;
  }
  public multiple() {
    return;
  }
  public percent() {
    return;
  }

  // Clear all data and stored Info
  public clearAll() {
    this.info = {
      disVal: "0",
      fVal: "",
      sVal: "",
      ops: ""
    };
    this.firstVal = 0;
    this.secondVal = 0;

    // to hold the numbers
    this.fNumber = [];
    this.sNumber = [];
    this.numFlag = true;
  }
  // Clear just the info
  public clearInfo() {
    this.secondVal = 0;

    // to hold the numbers
    this.fNumber = [];
    this.sNumber = [];
    // this.numFlag = true;
  }

  public equals() {
    this.info.disVal = (this.firstVal + this.secondVal).toString();
    return;
  }
  public decimal() {
    return;
  }
}
