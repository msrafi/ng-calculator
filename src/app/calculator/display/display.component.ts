import { Component, OnInit } from '@angular/core';
import { CalculatorService } from '../calculator.service';
import { Infos } from '../models/info.mode';

@Component({
  selector: "app-display",
  templateUrl: "./display.component.html",
  styleUrls: ["./display.component.css"]
})
export class DisplayComponent implements OnInit {
  info = {
    disVal: "0",
    fVal: "",
    sVal: "",
    ops: ""
  };

  constructor(private _calculatorService: CalculatorService) {}

  ngOnInit() {
    this._calculatorService.data$.subscribe((info: Infos) => {
      this.info = info;
      // console.log(info, this.info);
    });
  }
}
