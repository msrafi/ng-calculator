import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Infos } from '../calculator/models/info.mode';

@Injectable()
export class CalculatorService {
  info = {
    disVal: "0",
    fVal: "",
    sVal: "",
    ops: ""
  };

  public infos = new BehaviorSubject<Infos>(this.info);
  data$ = this.infos.asObservable();

  constructor() {}

  updateValue(info) {
    this.infos.next(info);
  }
}
